#!/bin/bash
home_dir="/etc/apache2"
IFS=. read ip1 ip2 ip3 ip4 <<< "$1"
nome_file="$ip1-$ip2-$ip3-$ip4"
rm $home_dir/rewrite_rules/negatives/$nome_file-test?.conf
rm $home_dir/rewrite_rules/positives/$nome_file-test?.conf

if [ $# -eq 2 ] 
    then
        service apache2 reload
fi