#!/bin/bash
IFS=. read ip1 ip2 ip3 ip4 <<< "$1"
home_dir="/etc/apache2"
nome_file="$ip1-$ip2-$ip3-$ip4"
printf "RewriteCond %sREMOTE_ADDR%s !^%s\\\.%s\\\.%s\\\.%s\$\nRewriteRule ^ - [S=1]\nProxyPassReverse \"^/\" \"http://%s.internal.lcl/\" [P,L]\n" "%{" "}" $ip1 $ip2 $ip3 $ip4 $2 > "$home_dir/rewrite_rules/negatives/$nome_file-$2.conf"
printf "#%s.%s.%s.%s - %s\n" $ip1 $ip2 $ip3 $ip4 $2 > "$home_dir/rewrite_rules/positives/$nome_file-$2.conf" 
printf "RewriteCond %sREMOTE_ADDR%s ^%s\\\.%s\\\.%s\\\.%s\$\nRewriteRule ^/(.*)$ http://%s.internal.lcl/ [P,L]\n"  "%{" "}" $ip1 $ip2 $ip3 $ip4 $2 >> "$home_dir/rewrite_rules/positives/$nome_file-$2.conf"

if [ $# -eq 3 ] 
    then
        service apache2 reload
fi
